# Ambisafe

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ambisafe', :git => 'https://bitbucket.org/ambisafe/client-ruby.git'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ambisafe

## Usage

TODO: Write usage instructions here

Initializer example:

```ruby
Ambisafe.configure do |config|
 config.ambisafe_server = 'http://ambisafe-server.host:8080'
 config.logger = Bitcoin::Logger.create("ambisafe", Bitcoin::Logger::Logger::LEVELS.index(:debug))
 config.secret = 'webapp_secret' # used for encryption of the keys that belong to this application
 config.api_key = 'API_KEY'
 config.api_secret = 'API_SECRET'
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release` to create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

1. Fork it ( https://github.com/[my-github-username]/ambisafe/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request