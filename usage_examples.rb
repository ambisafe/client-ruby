# 1. Configuration & init

# ... todo ...

# configuration variables:
# 1. application encryption password
# 2. key for hmac authentication
# [optional] 3. default security schema
# [optional] 4. default currency


# 2. Account creation

# containers is a hash with container jsons
containers = {
    user : JSON.decode(params[:walletContainer]),
    operator : Ambisafe.generate_key_and_build_container
}
account = Account.new # account created with web application business logic (...). Account model also pre-defined by the app.

# last parameter is optional, by default it's BTC
# the created account will be used for BTC and omni core
address = Ambisafe.create_account(account.id, containers, 'Wallet4', 'BTC') # API call to Ambisafe happens here
account.address = address
account.save

# the same workflow for update account besides .create_account
address = Ambisafe.update_account(account.id, containers, 'Wallet4', 'BTC') # API call to Ambisafe happens here


# 3. get the balance of account
Ambisafe.get_balance("id2", "BTC")

# 4. Sign transaction and submit to network.
# Transaction, received from browser includes sighases array.
# Get the container with private key from server
account = Ambisafe::get_account(account_id, currency)
priv_key = Ambisafe::decrypt_priv_key account

# Update sighashes with signature.
signed_sighahses = Ambisafe.sign(sighahses, priv_key)

# Send signed sighashes together with user's signed sighahses and raw tx hex to the server
transaction["user_signatures"] = user_signed_sighashes
transaction["operator_signatures"] = signed_sighahses

# It's safe to send signed sighash via the network
Ambisafe.submit_transaction(account_id, transaction, "BTC")


# what happens above:
# 1. fetch operator container for account_id
# 2. decrypt operator container with application password
# 3. co-sign transaction with operator private key
# 4. submit transaction to Ambisafe

# 5. send transaction received from browser without cosigning

transaction_id = Ambisafe.submit(transaction_hex, account_id)

# Request account_id, currency, destination and amount to build non-signed transaction
transaction = Ambisafe.build_transaction(account_id, "MP31", "1FQSHsxjq3Ui8Grtg1dWkhCYAK3AWg3mmR", "2.47")
# returned JSON includes tx raw hex, fee and array of sighashes (One sighahs per tx input).
# {"hex"=>"010000000176...", "fee"=>"0.00001 BTC", "sighashes"=>["73020cb8c25f434e00473dcd71be5bcfc0adaf107e0b0f36499d5abf8bd2da18"]}

# Get balance of any address for specified currency. The currency must be implemented on the server side
crypto_currency_balance = Ambisafe.get_address_balance("BTC", "1FQSHsxjq3Ui8Grtg1dWkhCYAK3AWg3mmR")