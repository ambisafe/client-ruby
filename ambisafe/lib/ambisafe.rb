require "ambisafe/version"
require "bitcoin"
require "yaml"
require "ambisafe/crypto"
require "ambisafe/authenticated_request"
require "ambisafe/configuration"

module Ambisafe
  class << self

    attr_writer :configuration

    def logger
      Ambisafe::config.logger
    end

    def config
      @config
    end

    def configure(&block)
      @config ||= Configuration.new(&block)
      yield @config
    end

    def create_account(account_id, containers, security_schema, currency = Ambisafe::config.currency)
      account = {"id" => account_id,
                 "currency" => currency,
                 "security_schema" => security_schema,
                 "containers" => containers}
      Ambisafe.logger.info "Sending to server..."
      response = Ambisafe::AuthenticatedRequest.make_and_execute('POST', '/accounts', account.to_json)['account']['address']
      Ambisafe.logger.info "Server response: #{response}"
      response
    end

    def update_account(account_id, containers, security_schema, additional_parameters, currency = Ambisafe::config.currency)
      account = {"id" => account_id,
                 "currency" => currency,
                 "security_schema" => security_schema,
                 "containers" => containers}
      account.merge! additional_parameters
      Ambisafe.logger.info "Sending to server..."
      response = Ambisafe::AuthenticatedRequest.make_and_execute('PUT', '/accounts', account.to_json)['account']['address']
      Ambisafe.logger.info "Server response: #{response}"
      response
    end

    def get_balance(account_id, currency)
      Ambisafe::AuthenticatedRequest.make_and_execute('GET', '/balances/' + currency + '/' + account_id)
    end

    def get_account(account_id, currency)
      Ambisafe::AuthenticatedRequest.make_and_execute('GET', '/accounts/' + account_id + '/' + currency)
    end

    def generate_key_and_build_container
      Ambisafe.logger.debug "Generating key pair and creating container"
      generate_key_and_build_user_container
    end

    # Will be deleted, used to mock user containers
    def generate_key_and_build_user_container(password = Ambisafe::config.secret)
      key = Bitcoin::Key.generate
      container = {"salt" => SecureRandom.uuid, "data" => key.priv, "public_key" => key.pub}
      Ambisafe.logger.debug "Encryption data for pub_key #{container[:public_key]} started"
      priv_key = container["data"].scan(/../).map(&:hex).pack('c*')
      encrypted_data = Crypto.encrypt(priv_key, container["salt"], password)
      container["iv"] = encrypted_data[0].unpack('H*')[0]
      container["data"] = encrypted_data[1].unpack('H*')[0]
      Ambisafe.logger.info "Container with pub_key = #{container["public_key"]} generated"
      container
    end

    def build_transaction(account_id, currency, destination, amount, options = {})
      Ambisafe.logger.debug "Sending build transaction request:"
      body = {
          "destination" => destination,
          "amount" => amount,
          "options" => {} # default_options => {feePerKb: KeyserverConfigFee, outputs => [], useUnconfirmed => true}
      }
      body['options'] = options if options.kind_of?(Hash) && !options.empty?
      # use config fee if transaction fee is unset:
      body['options']['feePerKb'] = Ambisafe::config.fee_per_kb if body['options']['feePerKb'].nil? && !Ambisafe::config.fee_per_kb.nil?

      Ambisafe.logger.debug body.inspect

      response = Ambisafe::AuthenticatedRequest.make_and_execute('POST', '/transactions/build/' + account_id + '/' + currency, body.to_json)
      Ambisafe.logger.debug "Server response: #{response}"
      response
    end

    def build_recovery_transaction(account_id, currency, address)
      response = Ambisafe::AuthenticatedRequest.make_and_execute('POST', '/transactions/build_recovery/' + account_id + '/' + currency + '/' + address)
      Ambisafe.logger.debug "Server response: #{response}"
      response
    end

    def submit_transaction(account_id, transaction, currency)
      puts transaction.to_json
      Ambisafe::AuthenticatedRequest.make_and_execute('POST', '/transactions/submit/' + account_id + '/' + currency, transaction.to_json)
    end

    # Used for Wallet 4.
    # {transaction_hex: '45g2g234g234g', fee: '0.0001 BTC' sighashes: ['13g353..', '3g45ag345g...']}
    def cosign_and_submit(transaction, account_id, currency)
      account = Ambisafe::get_account(account_id, currency)
      priv_key = decrypt_priv_key account
      Ambisafe.logger.debug "Private key decrypted"
      transaction["operator_signatures"] = sign(transaction["sighashes"], priv_key)
      Ambisafe.logger.info "Sighashes signed"
      transaction
    end

    def sign(sighashes, priv_key)
      sighashes.map { |sighash| sign_hash(sighash, priv_key) }
    end

    def decrypt_priv_key(account)
      operator_container = account["containers"]["OPERATOR"]
      decrypt_priv_key_from_container(operator_container)
    end

    def decrypt_priv_key_from_container(container, password = Ambisafe::config.secret)
      data = container["data"].scan(/../).map(&:hex).pack('c*')
      iv = container["iv"].scan(/../).map(&:hex).pack('c*')
      priv_key = Ambisafe::Crypto.decrypt(data, container["salt"], iv, password)
      priv_key.unpack('H*')[0]
    end

    def get_address_balance(currency, address)
      Ambisafe::AuthenticatedRequest.make_and_execute('GET', '/balances/' + currency + '/address/' + address)
    end

    private

    def sign_hash(sighash, priv_key)
      keypair = Bitcoin.open_key priv_key
      sig = Bitcoin.sign_data(keypair, [sighash].pack("H*"))
      sig.unpack("H*").first
    end
  end
end