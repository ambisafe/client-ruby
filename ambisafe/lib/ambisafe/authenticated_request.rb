require 'openssl'
require 'rest_client'
require 'json'
require 'base64'

module Ambisafe
  class AuthenticatedRequest
    def self.make_and_execute(method, uri, body=nil)
      url = Ambisafe::config.ambisafe_server + uri
      nonce = (Time.now.getutc.to_f * 1e3).to_i
      message = nonce.to_s + "\n" + method + "\n" + url + "\n" + body.to_s
      signature = Base64.encode64("#{OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha512'), Ambisafe::config.api_secret, message)}").chomp.gsub(/\n/, '')

      headers = {"API-Key" => Ambisafe::config.api_key,
                 "Signature" => signature,
                 "timestamp" => nonce}

      begin
        case method
          when 'GET'
            response = RestClient.get(url, headers.merge({:accept => :json}))
          when 'POST'
            response = RestClient.post(url, body, headers.merge({:content_type => :json, :accept => :json}))
          when 'PUT'
            response = RestClient.put(url, body, headers.merge({:content_type => :json, :accept => :json}))
          else
            raise "Method '#{method}' is not supported"
        end
        JSON.parse(response)
      rescue => e
        Ambisafe.logger.error e.message
        if e.respond_to?("response")
          Ambisafe.logger.error e.response
        end
        raise e
      end
    end
  end
end