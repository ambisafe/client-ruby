require 'openssl'
require 'rest_client'
require 'json'
require 'base64'

module Ambisafe
  class Crypto
    ITERATIONS = 1000
    KEY_LENGTH = 32
    DIGEST_SHA512 = OpenSSL::Digest::SHA512.new

    def self.encrypt(data, salt, password = Ambisafe::config.secret)
      begin
        cipher = OpenSSL::Cipher::AES.new(256, :CBC)
        cipher.encrypt
        cipher.key = derive_key(salt, password)
        iv = cipher.random_iv
        data = cipher.update(data) + cipher.final
        return iv, data
      rescue OpenSSL::Cipher::CipherError => e
        Ambisafe.logger.error "Encryption failed"
        Ambisafe.logger.error e.message + "\n " + e.backtrace.join("\n ")
        e.message
      end
    end

    def self.decrypt(encrypted, salt, iv, password = Ambisafe::config.secret)
      begin
        decipher = OpenSSL::Cipher::AES.new(256, :CBC)
        decipher.decrypt
        decipher.key = derive_key(salt, password)
        decipher.iv = iv
        decrypted = decipher.update(encrypted) + decipher.final
        decrypted
      rescue OpenSSL::Cipher::CipherError => e
        Ambisafe.logger.error "Decryption failed"
        Ambisafe.logger.error e.message + "\n " + e.backtrace.join("\n ")
        e.message
      end
    end

    private
    def self.derive_key(salt, password)
      OpenSSL::PKCS5.pbkdf2_hmac(password, salt, ITERATIONS, KEY_LENGTH, DIGEST_SHA512)
    end
  end
end