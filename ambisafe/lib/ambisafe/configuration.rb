module Ambisafe
  class Configuration
    NOT_CONFIGURED_EXCEPTION = "GEM is not configured. Check README to setup configuration"

    attr_writer :ambisafe_server, :secret, :api_key, :api_secret
    attr_accessor :logger, :currency, :fee_per_kb

    def initialize
      # Http URL of the Ambisafe KeyServer
      @ambisafe_server = nil

      # Secret phrase which is used to encrypt/decrypt private keys
      @secret = nil

      # Api_key for HMAC-SHA512 authentication
      @api_key = nil

      # Fee per kilobyte of BTC transaction
      @fee_per_kb = nil

      # Secret for HMAC-SHA512 authentication
      @api_secret = nil

      # Default logger puts the output in ambisafe.log with log_level :debug
      @logger = Bitcoin::Logger.create("ambisafe", Bitcoin::Logger::Logger::LEVELS.index(:debug))

      # For most clients there will be one cryptocurrency family. Therefore currency is constant for some requests
      @Currency = "BTC"

    end

    def ambisafe_server
      @ambisafe_server ||= raise NOT_CONFIGURED_EXCEPTION
    end

    def secret
      @secret ||= raise NOT_CONFIGURED_EXCEPTION
    end

    def api_key
      @api_key ||= raise NOT_CONFIGURED_EXCEPTION
    end

    def api_secret
      @api_secret ||= raise NOT_CONFIGURED_EXCEPTION
    end
  end
end