require "ambisafe"
require 'rspec'

Ambisafe.configure do |config|
  config.ambisafe_server = 'http://localhost:8080'
  config.logger = Bitcoin::Logger.create("ambisafe", Bitcoin::Logger::Logger::LEVELS.index(:debug))
  config.secret = 'ololo'
  config.api_key = 'demo'
  config.api_secret = 'demo'
  config.currency = 'BTC'
end