require 'spec_helper'

describe "Ambisafe" do
  it "creates container" do
    container = Ambisafe::generate_key_and_build_container
    puts container
    expect(container['salt']).not_to be_empty
    expect(container['data']).not_to be_empty
    expect(container['public_key']).not_to be_empty
    expect(container['iv']).not_to be_empty
  end

  it "decrypts priv key from container" do
    container = {"salt" => "ca20faef-ac3f-40a6-99c0-500855c03207",
                 "data" => "a0b0cbf2c2697f5141041da8a012149dc4cd82df6f43be8cfc58342ba8e663722178509b667217f2c990ec24ffaeb2ed",
                 "public_key" => "034a94cacac4327feb793047c514b256b326c3c474d73c861407a8709f9901039e",
                 "iv" => "ff55e03b11dc43adf839c3aee3632b36"}
    priv_key = Ambisafe::decrypt_priv_key_from_container(container, "test")
    expect(priv_key).to eq("8a3167b6032285a9fd89fcf9110d51ce1cffaf0eb21bc316560d0e510ebac7cd")
  end
end