require 'spec_helper'

module Ambisafe
  describe Configuration do
    it "default value raises the error" do
      expect { Configuration.new.ambisafe_server }.to raise_error "GEM is not configured. Check README to setup configuration"
    end

    it "set keyserver_url" do
      config = Configuration.new
      config.ambisafe_server = "http://localhost:8080"
      expect(config.ambisafe_server).to eq("http://localhost:8080")
    end

    it "set the secret" do
      Ambisafe.configure do |config|
        config.secret = 'test'
        expect(config.secret).to eq("test")
      end
    end

  end
end