require 'spec_helper'
describe "AES" do
  it 'AES 256 with pbkdf2_hmac_SHA512 1000 iterations key' do
    key = Bitcoin::generate_key
    salt = SecureRandom.uuid
    priv_key = key[0]
    public_key = key[1]

    encrypted_data = Ambisafe::Crypto.encrypt(priv_key, salt, "pass")
    iv = encrypted_data[0]
    encrypted_priv_key = encrypted_data[1]

    decrypted_priv_key = Ambisafe::Crypto.decrypt(encrypted_priv_key, salt, iv, "pass")

    expect(priv_key).to eq decrypted_priv_key
    expect(priv_key).not_to eq encrypted_priv_key
  end
end

