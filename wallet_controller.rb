class WalletController < ApplicationController
  def info
    render :json => {:api_version => "0.1"}
  end

  def get_balance
    render :json => Ambisafe.get_balance(:BTC, current_user.id)
  end

  def build_transaction
    render :json => Ambisafe.build_transaction(current_user.id, :BTC, destination, amount)
  end

  def submit_transaction
    render :json => Ambisafe.cosign_and_submit(post[:transaction_hex], current_user.id)
  end
end
